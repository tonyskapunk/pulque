# pulque

Pulque: /ˈpʊl.keɪ/, /ˈpʊl.ki/ (Classical Nahuatl: metoctli), or octli, is an alcoholic beverage made from the fermented sap of the maguey (agave) plant. It is traditional to central Mexico, where it has been produced for millennia